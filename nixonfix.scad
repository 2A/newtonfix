/*
Copyright (c) 2017 Antti Ainasoja

This work is licensed under a Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International License.
http://creativecommons.org/licenses/by-nc-sa/4.0/
*/

// $fn=1;
$fa=2;
$fs=0.1;

module torus(major, minor) {
    minkowski() {
        difference() {
            cylinder(0.01, major, major);
            cylinder(0.01, major-0.01, major-0.01);
        }
        sphere(minor);
    }
}

module lug() {
    difference() {
        union() {
            intersection() {
                scale([1,1,0.75]) sphere(4);
                translate([-2,0,0]) cube(10,10,10);
            }
            scale([1,1,0.25]) sphere(4);
            scale([1,1,0.25]) rotate([0,-90,0]) cylinder(2,4,4);
        }
        translate([2.2,0,0.6]) scale([0.5, 0.75, 0.5]) sphere(1);
        translate([-5,-10,-5]) cube(size = [10,10,10]);
    }
}

module hole() {
    rotate([0,0,90]) difference() {
        union() {
            translate([-16,-15,-1]) cube(size = [32,30,5]);
            translate([-15,-16,-1]) cube(size = [30,32,5]);
            translate([ -15, -15, -1]) cylinder(5,1,1);
            translate([ 15, -15, -1]) cylinder(5,1,1);
            translate([ -15, 15, -1]) cylinder(5,1,1);
            translate([ 15, 15, -1]) cylinder(5,1,1);
        }
        union() {
            translate([ -11.5, -17, -2]) cylinder(7,2,2);
            translate([ 11.5, -17, -2]) cylinder(7,2,2);
            translate([ -11.5, 17, -2]) cylinder(7,2,2);
            translate([ 11.5, 17, -2]) cylinder(7,2,2);
        }
    }
}

module bezel() {
    union() {
        translate([ 15, 15, 0]) cylinder(3,4,4);
        translate([ 15,-15, 0]) cylinder(3,4,4);
        translate([-15,-15, 0]) cylinder(3,4,4);
        translate([-15, 15, 0]) cylinder(3,4,4);
        translate([-19,-15, 0]) cube(size = [38,30,3]);
        translate([-15,-19, 0]) cube(size = [30,38,3]);
        translate([19,10,0]) lug();
        translate([19,-10,0]) scale([1,-1,1]) lug();
        rotate([0, 0, 180])translate([19,10,0]) lug();
        rotate([0, 0, 180]) translate([19,-10,0]) scale([1,-1,1]) lug();
    }
}

module cover() {
    difference() {
        union() {
            translate([-15,-15, 0]) minkowski() {
                cylinder(0.01,3,3);
                sphere(1);
            }
            translate([-15,15, 0]) minkowski() {
                cylinder(0.01,3,3);
                sphere(1);
            }
            translate([15,-15, 0]) minkowski() {
                cylinder(0.01,3,3);
                sphere(1);
            }
            translate([15,15, 0]) minkowski() {
                cylinder(0.01,3,3);
                sphere(1);
            }
            /*
            hull() {
                translate([-15,-18,0]) sphere(1);
                translate([15,-18,0]) sphere(1);
                translate([15,18,0]) sphere(1);
                translate([-15,18,0]) sphere(1);
                translate([18,-15,0]) sphere(1);
                translate([18,15,0]) sphere(1);
                translate([-18,15,0]) sphere(1);
                translate([-18,-15,0]) sphere(1);
            }
            */
            translate([-15,-18,0]) minkowski() {
                cube([30,36,0.01]);
                sphere(1);
            }
            translate([-18,-15,0]) minkowski() {
                cube([36,30,0.01]);
                sphere(1);
            }


        }
        translate([-17.5, -17.5, 0]) cube(35,35,2);
    }
}

module seal() {
    union() {
       translate([10,10,0]) intersection() {
            torus(4, 0.25);
            translate([0,0,-1]) cube([10,10,2]);
       }
       rotate([0,0,90]) translate([10,10,0]) intersection() {
            torus(4, 0.25);
            translate([0,0,-1]) cube([10,10,2]);
       }
       rotate([0,0,180]) translate([10,10,0]) intersection() {
            torus(4, 0.25);
            translate([0,0,-1]) cube([10,10,2]);
       }
       rotate([0,0,-90]) translate([10,10,0]) intersection() {
            torus(4, 0.25);
            translate([0,0,-1]) cube([10,10,2]);
       }
       translate([-10,-14,0]) rotate([0,90,0]) cylinder(20, 0.25, 0.25);
       translate([-14,-10,0]) rotate([0,90,90]) cylinder(20, 0.25, 0.25);
       translate([-10,14,0]) rotate([0,90,0]) cylinder(20, 0.25, 0.25);
       translate([14,-10,0]) rotate([0,90,90]) cylinder(20, 0.25, 0.25);
   }
}
module guide() {
    union() {
       translate([10,10,0]) intersection() {
            torus(3.5, 0.25);
            translate([0,0,-1]) cube([10,10,2]);
       }
       rotate([0,0,90]) translate([10,10,0]) intersection() {
            torus(3.5, 0.25);
            translate([0,0,-1]) cube([10,10,2]);
       }
       rotate([0,0,180]) translate([10,10,0]) intersection() {
            torus(3.5, 0.25);
            translate([0,0,-1]) cube([10,10,2]);
       }
       rotate([0,0,-90]) translate([10,10,0]) intersection() {
            torus(3.5, 0.25);
            translate([0,0,-1]) cube([10,10,2]);
       }
       translate([-10,-13.5,0]) rotate([0,90,0]) cylinder(20, 0.25, 0.25);
       translate([-13.5,-10,0]) rotate([0,90,90]) cylinder(20, 0.25, 0.25);
       translate([-10,13.5,0]) rotate([0,90,0]) cylinder(20, 0.25, 0.25);
       translate([13.5,-10,0]) rotate([0,90,90]) cylinder(20, 0.25, 0.25);
   }
}

module screwHole(x,y,z) {
    translate([x, y , z-5]) union() {
        cylinder(5.1,0.5, 0.5);
        translate([0,0,5]) cylinder(5,1 , 1);
    }
}

module screwHoles() {
    union() {
        screwHole(-11.5,-17,-0.0);
        screwHole(-11.5,17,-0.0);
        screwHole(11.5,-17,-0.0);
        screwHole(11.5,17,-0.0);

        screwHole(-13.75,-13.75,0.5);
        screwHole(-13.75,13.75,0.5);
        screwHole(13.75,-13.75,0.5);
        screwHole(13.75,13.75,0.5);
    }
}

translate([0,0,1]) difference () {
    union() {
        difference() {
            bezel();
            hole();
        }
        cover();
        guide();
    }
    seal();
    rotate([180,0,90]) screwHoles();
}
