# Nixon Newton fix #

3D-printable back plate with 20 mm slugs for [Nixon Newton](http://www.nixon.com/fi/en/newton/A116.html) watch. The part enables replacing the silicon strap with generic wristbands.

Watch measurements were made using a cheap plastic ruler and estimating by eyesight.

Part was modelled using [OpenSCAD](http://www.openscad.org/) and sliced for Markforged Onyx/Mark II printer with Onyx filament.

## Caveats/Notes/Bugs/etc.:
* The cut out for rubber seal is too small. As a result the rubber seal wont fit in the case.
* The bezel is a little low. A small gap is left between watch case and the part. A piece of paper fits in, but a nail does not.  
* holes for screws needed to be drilled slightly for assembly.
* 20 mm Pins and straps fit well, but wether the slugs are sturdy enough for use remains to be seen.

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)

[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
